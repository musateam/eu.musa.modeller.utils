package eu.musa.modeller.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class WriteTextFile {
	private Charset charset = StandardCharsets.UTF_8;
	private FileWriter fw = null;
	private BufferedWriter bw = null;
	private PrintWriter out = null;
	
	
	public WriteTextFile() { }
	
	void OpenFile(String file) {
	    try {
			fw = new FileWriter(file, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    bw = new BufferedWriter(fw);
	    out = new PrintWriter(bw);
	}
	
	void CloseFile()
	{
	    if(out != null) out.close();
		try {
		    if(bw != null) bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
		    if(fw != null) fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} // CloseFile()
	
	void AddLine(String line)
	{
		out.println(line);
	} // AddLine()
	
	void replaceLine(String file, String line, String newLine)
	{
		String content;
		try {
			Path path = Paths.get(file);
			
			content = new String(Files.readAllBytes(path), charset);
			content = content.replaceAll(line, newLine);
			Files.write(path, content.getBytes(charset));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
