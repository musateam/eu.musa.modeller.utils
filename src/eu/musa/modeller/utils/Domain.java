package eu.musa.modeller.utils;

public class Domain {
	    private String id;	    
		private String name;

	    public Domain() {}
	    
	    public Domain(String id, String name) {
	    	this.id = id;
	    	this.name = name;
	    }	    

	    public String getId() {
			return id;
		}

	    public void setId(String id) {
			this.id = id;
		}   		
	    
		public String getName() {
	        return name;
	    }
	 
	    public void setName(String value) {
	        this.name = value;
	    }
}
