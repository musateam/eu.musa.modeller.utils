package eu.musa.modeller.utils;


public class Controls {
	    private Long id;	
	    private String controlId;
		private String name;
	    private String specification;
	    private String domainId;	    
	    private String domain;
	    private int control;
	    private int enhancement;

	    public Controls() {}
	    
	    public Controls(Long id, String controlId, String name, String specification, String domainId, String domain, int control, int enhancement) {
	    	this.id = id;
	    	this.controlId = controlId;
	    	this.name = name;
	    	this.specification = specification;
	    	this.domainId = domainId;
	    	this.domain = domain;
	    	this.control = control;
	    	this.enhancement = enhancement;
	    }
	 	    
	    public Long getId() {
			return id;
		}

	    public void setId(Long id) {
			this.id = id;
		}   		

	    public String getControlId() {
			return controlId;
		}

	    public void setControlId(String controlId) {
			this.controlId = controlId;
		}   		
	    
		public String getName() {
	        return name;
	    }
	 
	    public void setName(String value) {
	        this.name = value;
	    }

	    public String getSpecification() {
	        return specification;
	    }
	 
	    public void setSpecification(String value) {
	        this.specification = value;
	    }
	    
	    public String getDomainId() {
			return domainId;
		}

	    public void setDomainId(String domainId) {
			this.domainId = domainId;
		}   		
	    
	    public String getDomain() {
	        return domain;
	    }

	    public void setDomain(String value) {
	        this.domain = value;
	    }

	    public int getControl() {
	        return control;
	    }

	    public void setControl(int value) {
	        this.control = value;
	    }

	    public int getEnhancement() {
	        return enhancement;
	    }

	    public void setEnhancement(int value) {
	        this.enhancement = value;
	    }
}
