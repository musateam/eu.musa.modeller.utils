package eu.musa.modeller.utils;

public class SC {
	    private Long id;	  
	    private String controlId;
		private String name;
	    private String specification;	    
	    private String domain;      
	    private String domainId;
   

	    public SC() {}
	    
	    public SC(Long id, String controlId, String name, String specification, String domain, String domainId) {
	    	this.id = id;
	    	this.controlId = controlId; 
	    	this.name = name;
	    	this.specification = specification;
	    	this.domain = domain;
	    	this.domainId = domainId;
	    }
	 	    
	    public Long getId() {
			return id;
		}

	    public void setId(Long id) {
			this.id = id;
		}   		

	    public String getControlId() {
			return controlId;
		}

	    public void setControlId(String controlId) {
			this.controlId = controlId;
		}   		
	    
		public String getName() {
	        return name;
	    }
	 
	    public void setName(String value) {
	        this.name = value;
	    }

	    public String getSpecification() {
	        return specification;
	    }
	 
	    public void setSpecification(String value) {
	        this.specification = value;
	    }
	    
	    
	    public String getDomain() {
	        return domain;
	    }

	    public void setDomain(String value) {
	        this.domain = value;
	    }

	    public String getDomainId() {
	        return domainId;
	    }

	    public void setDomainId(String value) {
	        this.domainId = value;
	    }
}
