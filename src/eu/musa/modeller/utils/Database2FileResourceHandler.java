package eu.musa.modeller.utils;

import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Scanner;

import java.util.List;

import javax.inject.Inject;

import org.camel_dsl.camel.CamelModel;
import org.camel_dsl.deployment.CapabilityLink;
import org.camel_dsl.deployment.DeploymentModel;
import org.camel_dsl.deployment.Hosting;
import org.camel_dsl.deployment.InternalComponent;
import org.camel_dsl.deployment.RequiredMUSACapability;
import org.camel_dsl.dsl.camelDsl.CamelPlusModel;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.diagnostics.Severity;
import org.eclipse.xtext.parser.IEncodingProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.web.server.IServiceContext;
import org.eclipse.xtext.web.server.model.IWebDocumentProvider;
import org.eclipse.xtext.web.server.model.IWebResourceSetProvider;
import org.eclipse.xtext.web.server.model.IXtextWebDocument;
import org.eclipse.xtext.web.server.model.XtextWebDocument;
import org.eclipse.xtext.web.server.persistence.IResourceBaseProvider;
import org.eclipse.xtext.web.server.persistence.IServerResourceHandler;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ObjectExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

// MCP
@SuppressWarnings("all")
public class Database2FileResourceHandler implements IServerResourceHandler {
	@Inject
	private IResourceBaseProvider resourceBaseProvider;

	@Inject
	private IWebResourceSetProvider resourceSetProvider;

	@Inject
	private IWebDocumentProvider documentProvider;

	@Inject
	private IEncodingProvider encodingProvider;

	@Inject
	IResourceValidator resourceValidator;

	private static SessionFactory factory;
	private static final String fileExtension = ".camel";
	private static final String UTF_8 = "UTF-8";
	private int contErrors = 0;

	/**
	 * Returns the appID
	 * 
	 * @param appNameIdExtension
	 * @return
	 */
	private String getAppID(String appNameIdExtension) {
		String appName = "";
		String appId = "";
		String appIdExtension = "";
		if (appNameIdExtension.indexOf("$") > 0) {
			appName = appNameIdExtension.substring(0, appNameIdExtension.indexOf("$"));
			appIdExtension = appNameIdExtension.substring(appNameIdExtension.indexOf("$") + 1);
		}
		ManageMCApp ME = new ManageMCApp(factory);
		int len = appIdExtension.indexOf(fileExtension);
		appId = appIdExtension.substring(0, len);
		return appId;
	}

	/**
	 * Returns a report of issues given a resource
	 * 
	 * @param xtextResource
	 * @return
	 */
	public String checkResource(XtextResource xtextResource) {
		int errorId = 0;
		String report = "";
		Resource resource = xtextResource;
		if (!resource.isLoaded())
			return "";
		contErrors = 0;
		// CheckMode.ALL CheckMode.NORMAL_AND_FAST
		// !issues.exists[severity == Severity.ERROR]
		List<Issue> issues = resourceValidator.validate(resource,
				// CheckMode.NORMAL_ONLY, CancelIndicator.NullImpl);
				CheckMode.ALL, CancelIndicator.NullImpl);
		for (Issue issue : issues) {
			if (issue.getSeverity() == Severity.ERROR) {
				errorId++;
				// System.out.println(errorId + ":" + issue.getLineNumber() +
				// ":" + " ERROR: " + issue.getMessage());
				// report += errorId+ ":" + issue.getLineNumber() + ":" + "
				// ERROR: " + issue.getMessage() + "\n";
				report += issue.getLineNumber() + ":" + " ERROR: " + issue.getMessage() + "\n";

				contErrors++;
			} else if (issue.getSeverity() == Severity.WARNING) {
				errorId++;
				report += issue.getLineNumber() + ":" + " WARNING: " + issue.getMessage() + "\n";
			} else if (issue.getSeverity() == Severity.INFO) {
				errorId++;
				report += issue.getLineNumber() + ":" + " INFO: " + issue.getMessage() + "\n";
			} else if (issue.getSeverity() == Severity.IGNORE) {
				errorId++;
				report += issue.getLineNumber() + ":" + " IGNORE: " + issue.getMessage() + "\n";
			}
		}

		for (EObject obj : xtextResource.getContents()) {			
			if (obj instanceof CamelPlusModel) {
				CamelPlusModel CM = (CamelPlusModel) obj;
				EList<DeploymentModel> plist = ((CamelPlusModel) obj).getDeploymentModels();
				DeploymentModel DM = plist.get(0); // solo hay un modelo!!!
				EList<InternalComponent> plistIC = DM.getInternalComponents();

			} // if(obj instanceof CamelModel)
		} // for (EObject obj : xtextResource.getContents()) {

		return report;
	}// checkResource(XtextResource xtextResource)

	/**
	 * Returns the current time stamp
	 * 
	 * @return
	 */
	private String getCurrentTimeStamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}

	Database2FileResourceHandler() {

		super();

		try {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
					// .configure("/info/java/tips/config/hibernate.cfg.xml").build();
					.configure("eu/musa/modeller/config/hibernate.cfg.xml").build();
			factory = new MetadataSources(registry).buildMetadata().buildSessionFactory();

			// factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}

	}

	/**
	 * Creates a model file given its id and content
	 * 
	 * @param resourceId
	 * @param content
	 * @param serviceContext
	 * @throws IOException
	 */
	public void createFile(String resourceId, String content, IServiceContext serviceContext) throws IOException {
		try {
			try {
				final URI uri = this.resourceBaseProvider.getFileURI(resourceId);
				ResourceSet _resourceSet = new ResourceSetImpl();
				URIConverter _uRIConverter = _resourceSet.getURIConverter();
				final OutputStream outputStream = _uRIConverter.createOutputStream(uri);
				String _encoding = this.encodingProvider.getEncoding(uri);
				final OutputStreamWriter writer = new OutputStreamWriter(outputStream, _encoding);
				String _text = content;
				writer.write(_text);
				writer.close();
			} catch (final Throwable _t) {
				if (_t instanceof WrappedException) {
					final WrappedException exception = (WrappedException) _t;
					throw exception.getCause();
				} else {
					throw Exceptions.sneakyThrow(_t);
				}
			}
		} catch (Throwable _e) {
			throw Exceptions.sneakyThrow(_e);
		}
	}

	public void createORupdate(String appNameIdExtension, String content, String valReport, int errValReport,
			IServiceContext serviceContext) throws IOException {
		String appName = "";
		String appId = "";
		String appIdExtension = "";
		if (appNameIdExtension.indexOf("$") > 0) {
			appName = appNameIdExtension.substring(0, appNameIdExtension.indexOf("$"));
			appIdExtension = appNameIdExtension.substring(appNameIdExtension.indexOf("$") + 1);
		}
		ManageMCApp ME = new ManageMCApp(factory);
		int len = appIdExtension.indexOf(fileExtension);
		appId = appIdExtension.substring(0, len);
		MCApp obj = ME.getMCApp(appId);
		// MCP
		int statusCamelReport = 2;
		if (obj != null) {
			ME.updateMCApp(Long.valueOf(obj.getId()), content, valReport, errValReport);
		} else {
			ME.addMCApp(appId, appName, "0", content, valReport, errValReport);
		}
		ME.updateStatusMCApp(Long.valueOf(appId), statusCamelReport);
	}

	/**
	 * Creates or updates a model file in xmi format given its id and content
	 * 
	 * @param appNameIdExtension
	 * @param content
	 * @param valReport
	 * @param errValReport
	 * @param serviceContext
	 * @throws IOException
	 */
	public void createORupdateXMI(String appNameIdExtension, String content, String valReport, int errValReport,
			IServiceContext serviceContext) throws IOException {
		String appName = "";
		String appId = "";
		String appIdExtension = "";
		if (appNameIdExtension.indexOf("$") > 0) {
			appName = appNameIdExtension.substring(0, appNameIdExtension.indexOf("$"));
			appIdExtension = appNameIdExtension.substring(appNameIdExtension.indexOf("$") + 1);
		}
		ManageMCApp ME = new ManageMCApp(factory);
		int len = appIdExtension.indexOf(fileExtension);
		appId = appIdExtension.substring(0, len);
		MCApp obj = ME.getMCApp(appId);
		// MCP
		int statusCamelReport = 2;
		if (obj != null) {
			ME.updateMCAppXMI(Long.valueOf(obj.getId()), content);
		} else {
			ME.addMCApp(appId, appName, "0", content, valReport, errValReport);
			ME.updateStatusMCApp(Long.valueOf(appId), statusCamelReport);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.xtext.web.server.persistence.IServerResourceHandler#get(java.
	 * lang.String, org.eclipse.xtext.web.server.IServiceContext)
	 */
	public XtextWebDocument get(String appNameIdExtension, final IServiceContext serviceContext) throws IOException {
		// appNameIdExtension=hola$4.camel
		String appName = "";
		String appId = "";
		String appIdExtension = "";
		if (appNameIdExtension.indexOf("$") > 0) {
			appName = appNameIdExtension.substring(0, appNameIdExtension.indexOf("$"));
			appIdExtension = appNameIdExtension.substring(appNameIdExtension.indexOf("$") + 1);
			appId = appIdExtension;
		} else {
			appId = appIdExtension;
		}
		try {
			try {
				String serviceType0 = serviceContext.getParameter("serviceType");				

				// if it doesn�t exist it is created
				URI uritmp = this.resourceBaseProvider.getFileURI(appNameIdExtension);
				/*
				 * IPath path = new Path(); IFile locationFile =
				 * ResourcesPlugin.getWorkspace().getRoot().getFile(path);
				 */
				if ((uritmp == null || !uritmp.isFile()) || !uritmp.isArchive()) {
					String serviceType = serviceContext.getParameter("serviceType");
					if (serviceType.contains("load")) // leer de BDD
					{
						String content = "";
						ManageMCApp ME = new ManageMCApp(factory);
						int len = appId.indexOf(fileExtension);						
						String idApplication = appId.substring(0, len);
						MCApp obj = ME.getMCApp(idApplication);
						if (obj == null)
							content = "";
						else
							content = obj.getCamel();
						createFile(appNameIdExtension, content, serviceContext);
					} else if (serviceType.contains("generate-all")) {
						
						// new file
						String content = "";
						String valReport = "";
						int errValReport = 0;
						int statusCamelReport = 2;
						ManageMCApp ME = new ManageMCApp(factory);
						int len = appId.indexOf(fileExtension);
						String idApplication = appId.substring(0, len);
						String name = appName.substring(0, len);
						Long objID1 = ME.addMCApp(idApplication, name, "0.0", content, valReport, errValReport);
						ME.updateStatusMCApp(Long.valueOf(idApplication), statusCamelReport);
						MCApp obj = ME.getMCAppNew(appName);
						createFile(appNameIdExtension, content, serviceContext);
					} else if (serviceType.contains("update")) {
						// dirty file
						String content = "";
						createFile(appNameIdExtension, content, serviceContext);
						
					} else {
						System.out.println("Database2FileResourceHandler::get: ERROR serviceType=." + serviceType);
					}

				}
				final URI uri = this.resourceBaseProvider.getFileURI(appNameIdExtension);
				if ((uri == null)) {					
					throw new IOException("The requested resource does not exist.");
				}
				final ResourceSet resourceSet = this.resourceSetProvider.get(appNameIdExtension, serviceContext);
				Resource _resource = resourceSet.getResource(uri, true);
				final XtextResource resource = ((XtextResource) _resource);
				XtextWebDocument _get = this.documentProvider.get(appNameIdExtension, serviceContext);
				final Procedure1<XtextWebDocument> _function = new Procedure1<XtextWebDocument>() {
					@Override
					public void apply(final XtextWebDocument it) {
						it.setInput(resource);
					}
				};
				return ObjectExtensions.<XtextWebDocument> operator_doubleArrow(_get, _function);
			} catch (final Throwable _t) {
				if (_t instanceof WrappedException) {
					final WrappedException exception = (WrappedException) _t;					
					throw exception.getCause();
				} else {					
					throw Exceptions.sneakyThrow(_t);
				}
			}
		} catch (Throwable _e) {
			System.out.println("Database2FileResourceHandler::get: ERROR  throwing Exceptions.sneakyThrow 2");
			throw Exceptions.sneakyThrow(_e);
		}
	}

	/**
	 * @param resourceSet
	 */
	void unloadAllResources(ResourceSet resourceSet) {
		for (Resource nextResource : resourceSet.getResources()) {
			nextResource.unload();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.xtext.web.server.persistence.IServerResourceHandler#put(org.
	 * eclipse.xtext.web.server.model.IXtextWebDocument,
	 * org.eclipse.xtext.web.server.IServiceContext)
	 */
	@Override
	public void put(final IXtextWebDocument document, final IServiceContext serviceContext) throws IOException {
		if (document == null)
			System.out.println(getCurrentTimeStamp() + "Database2FileResourceHandler::put: resourceId is null");
		else
			System.out.println(getCurrentTimeStamp() + "Database2FileResourceHandler::put: resourceId="
					+ document.getResourceId() + ".");
		try {
			try {
				String serviceType = serviceContext.getParameter("serviceType");				
				String _resourceId = document.getResourceId();

				final URI uri = this.resourceBaseProvider.getFileURI(_resourceId);
				XtextResource _resource = document.getResource();

				if (_resource.getContents().isEmpty())
					System.out.println(
							"Database2FileResourceHandler::put: test 0 - ERROR the camel file doesn't contain any object");

				ManageMCApp ME = new ManageMCApp(factory);
				// int statusCamelReport = 1;
				String appID = getAppID(_resourceId);				

				String valReport = checkResource(_resource);
				int errValReport = contErrors;

				if (_resource.getContents().isEmpty())
					System.out.println(
							"Database2FileResourceHandler::put: test 1 - ERROR the camel file doesn't contain any object");

				ResourceSet _resourceSet = _resource.getResourceSet();
				URIConverter _uRIConverter = _resourceSet.getURIConverter();
				final OutputStream outputStream = _uRIConverter.createOutputStream(uri);
				String _encoding = this.encodingProvider.getEncoding(uri);
				final OutputStreamWriter writer = new OutputStreamWriter(outputStream, _encoding);
				String _text = document.getText();
				writer.write(_text);
				writer.close();

				if (_resource.getContents().isEmpty())
					System.out.println(
							"Database2FileResourceHandler::put: test 2 - ERROR the camel file doesn't contain any object");

				createORupdate(_resourceId, _text, valReport, errValReport, serviceContext);

				if (_resource.getContents().isEmpty())
					System.out.println(
							"Database2FileResourceHandler::put: test 3 - ERROR the camel file doesn't contain any object");

				// save in XMI format
				String uriXMI = uri.toString();
				uriXMI = uriXMI.replace(".camel", ".xmi");

				String fileName = uriXMI.substring(uriXMI.indexOf("/"), uriXMI.length());

				// MCP if (!new File (fileName).exists()){ as� no puede ser
				// porque siempre hay que actualizar el contenido del fichero
				// xmi				
				Resource xmiTmp = _resourceSet.getResource(URI.createURI(uriXMI), false);
				if (xmiTmp != null)
					System.out.println("Database2FileResourceHandler::put: the resource already exits");
				Resource xmiResource = _resourceSet.createResource(URI.createURI(uriXMI));
				if (xmiResource == null)
					System.out.println("Database2FileResourceHandler::put: ERROR resource not created");
				// MCP: si esta vacio es que el usuario no ha cambiado nada del
				// fichero asi que hay que salir de aqui
				if (_resource.getContents().isEmpty()) {
					System.out.println(
							"Database2FileResourceHandler::put: ERROR the camel file doesn't contain any object");
					if (!_resource.isLoaded())
						System.out.println("Database2FileResourceHandler::put: ERROR the camel file is not loaded");
					// OJO MCP!!! borrar el fichero
					xmiResource.unload();					
					xmiResource.delete(Collections.EMPTY_MAP);					
					return;
				} else {
					xmiResource.getContents().add(_resource.getContents().get(0));
				}
				xmiResource.save(null);
				// MCP }
				// saltar "file:/"
				FileInputStream fileStream = new FileInputStream(fileName);
				String _textXMI = new Scanner(fileStream, UTF_8).useDelimiter("\\A").next();
				fileStream.close();

				// MCP requested 2017/03 manualmente reparar errores: cambiar
				// ".camel" por ".xmi", ...
				_textXMI = _textXMI.replaceAll("\\.camel", "\\.xmi");
				_textXMI = _textXMI.replaceAll("www\\.xmi-dsl", "www\\.camel-dsl");
				if (System.getProperty("os.name").contains("Windows")) {
					_textXMI = _textXMI.replaceAll("D:/MUSA/db-files", "\\.");
					_textXMI = _textXMI.replaceAll("d:/MUSA/db-files", "\\.");
				} else {
					_textXMI = _textXMI.replaceAll("/home/tecnalia/MUSA/db-files", "\\.");
				}

				createORupdateXMI(_resourceId, _textXMI, valReport, errValReport, serviceContext);
				
				// OJO MCP!!! borrar el fichero
				xmiResource.unload();
				// unloadAllResources(_resourceSet);				
				/*
				 * MCP: casca primera vez que intento salvar
				 * EcoreUtil.remove(_resourceSet.getEObject(URI.createURI(uriXMI
				 * ), false)); System.out.println(
				 * "Database2FileResourceHandler::put: resource removed!");
				 */
				// xmiResource = _resourceSet.getResource(URI.createURI(uriXMI),
				// false);
				xmiResource.delete(Collections.EMPTY_MAP); // funciona???								
			} catch (final Throwable _t) {
				if (_t instanceof WrappedException) {
					final WrappedException exception = (WrappedException) _t;
					System.out.println("Database2FileResourceHandler::put: ERROR  throwing " + exception.getCause());
					throw exception.getCause();
				} else {					
					throw Exceptions.sneakyThrow(_t);
				}
			}
		} catch (Throwable _e) {			
			throw Exceptions.sneakyThrow(_e);
		}
	}
}
