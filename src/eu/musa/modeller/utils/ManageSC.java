package eu.musa.modeller.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

public class ManageSC {
	private static WriteTextFile fich = null;

	private static SessionFactory factory = null;
	private static final String fileExtension = ".camel";

	/** For testing purposes
	 * @param args
	 */
	public static void main(String[] args) {
		SessionFactory fact = null;
		try {
			if (factory == null) {
				StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
						.configure("/eu/musa/modeller/config/hibernate.cfg.xml").build();
				fact = new MetadataSources(registry).buildMetadata().buildSessionFactory();

				factory = fact;
				// factory = new
				// Configuration().configure().buildSessionFactory();
			}
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}

		/* open file at the end of the file */
		String varResourceBaseProviderDB = "/home/tecnalia/MUSA/db-files";
		if (System.getProperty("os.name").contains("Windows")) {
			varResourceBaseProviderDB = "D:/MUSA/db-files";
		}
		String resourceBaseProviderDB = varResourceBaseProviderDB;
		String file = resourceBaseProviderDB + "/MUSAAGENTS" + fileExtension;
		fich = new WriteTextFile();
		// eliminar "}}"
		fich.replaceLine(file, "}}", "\n");
		fich.OpenFile(file);

		ManageSC ME = new ManageSC(fact);

		/* Read domains from DB */
		List Dl = ME.getDomains();

		/* Add domains to MUSAAGENTS.camel file */
		for (int i = 0; i < Dl.size(); i++) {
			Domain obj = (Domain) Dl.get(i);
			ME.addDomain(obj.getId(), obj.getName());
		}

		/* Read security controls from DB */
		List SCl = ME.getSCs();

		/* Add security controls to MUSAAGENTS.camel file */
		for (int i = 0; i < SCl.size(); i++) {
			SC obj = (SC) SCl.get(i);
			ME.addSC(obj.getControlId(), obj.getName(), obj.getSpecification(), obj.getDomainId());
		}

		// annadir "}}"
		fich.AddLine("}}");

		// close file
		fich.CloseFile();
		fich = null;
	}

	public ManageSC() {
		SessionFactory fact = null;
		try {
			if (factory == null) {
				StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
						.configure("/eu/musa/modeller/config/hibernate.cfg.xml").build();
				fact = new MetadataSources(registry).buildMetadata().buildSessionFactory();

				factory = fact;
				// factory = new
				// Configuration().configure().buildSessionFactory();
			}
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public ManageSC(SessionFactory fact) {
		factory = fact;
	}

	/** Method to CREATE a domain into MUSAAGENTS.camel file
	 * @param id
	 * @param name
	 */
	public void addDomain(String id, String name) {
		fich.AddLine("  domain " + id + " { name: \"" + name + "\" } ");
	}

	/** Method to READ all the records
	 * @return
	 */
	public List getDomains() {
		List lista = new ArrayList<Domain>();

		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();

			Query distinctDomains = session
					.createQuery("SELECT ci.domainId, ci.domain FROM Controls ci GROUP BY ci.domainId");
			List lista2 = distinctDomains.getResultList();
			for (Iterator iterator = lista2.iterator(); iterator.hasNext();) {
				Object obj[] = (Object[]) iterator.next();
				String domainId = (String) obj[0];
				String domain = (String) obj[1];
				lista.add(new Domain(domainId, domain));
			}

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}

		return lista;
	} // getDomains()

	/** Method to CREATE a security control into MUSAAGENTS.camel file
	 * @param id
	 * @param name
	 * @param specification
	 * @param domain
	 */
	public void addSC(String id, String name, String specification, String domain) {
		fich.AddLine("  security control " + id + " { ");
		fich.AddLine("  name: \"" + name + " \"");
		fich.AddLine("  specification: \"" + specification + " \"");
		fich.AddLine("  domain: MUSASEC." + domain + " ");
		fich.AddLine("  } ");
	}

	/** Method to READ all the records
	 * @return
	 */
	public List getSCs() {
		List lista = new ArrayList<SC>();

		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			List lista2 = session.createQuery("FROM Controls").list();
			for (Iterator iterator = lista2.iterator(); iterator.hasNext();) {
				Controls obj = (Controls) iterator.next();
				lista.add(new SC(obj.getId(), obj.getControlId(), obj.getName(), obj.getSpecification(),
						obj.getDomain(), obj.getDomainId()));
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}

		return lista;
	} // getSCs()
}