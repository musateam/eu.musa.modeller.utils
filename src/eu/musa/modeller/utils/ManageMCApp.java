package eu.musa.modeller.utils;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

public class ManageMCApp {
	private static SessionFactory factory;
	private static final String fileExtension = ".camel";

	/**
	 * For testing purposes
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SessionFactory fact = null;
		try {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
					// .configure("/info/java/tips/config/hibernate.cfg.xml").build();
					.configure("/eu/musa/modeller/config/hibernate.cfg.xml").build();
			fact = new MetadataSources(registry).buildMetadata().buildSessionFactory();

			// factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		ManageMCApp ME = new ManageMCApp(fact);

		/* List down all the records */
		ME.listMCApps();

		/* List down new list of the records */
		ME.listMCApps();
	}

	public ManageMCApp() {
		SessionFactory fact = null;
		try {
			StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
					// .configure("/info/java/tips/config/hibernate.cfg.xml").build();
					.configure("/eu/musa/modeller/config/hibernate.cfg.xml").build();
			fact = new MetadataSources(registry).buildMetadata().buildSessionFactory();

			// factory = new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
		factory = fact;
	}

	public ManageMCApp(SessionFactory fact) {
		factory = fact;
	}

	/**
	 * Method to CREATE an mcApp in the database
	 * 
	 * @param appId
	 * @param appName
	 * @param version
	 * @param camel
	 * @param camelReport
	 * @param errorsCamelReport
	 * @return
	 */
	public Long addMCApp(String appId, String appName, String version, String camel, String camelReport,
			int errorsCamelReport) {
		Session session = null;
		Transaction tx = null;
		Long objID = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();

			MCApp obj = new MCApp();
			obj.setId(new Long(appId));
			obj.setMCApplication(appName);
			obj.setMCAVersion(version);
			obj.setCamel(camel);
			obj.setDate(new Date());
			obj.setCamelReport(camelReport);
			obj.setErrorsCamelReport(errorsCamelReport);
			// obj.setStatusCamelReport(statusCamelReport);

			objID = (Long) session.save(obj);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
		return objID;
	}

	
	/** Method to READ all the records
	 * 
	 */
	public void listMCApps() {
		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			List list = session.createQuery("FROM MCApp").list();
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				MCApp obj = (MCApp) iterator.next();

			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
	}

	/* Method to READ all the records */
	public List getMCApps() {
		List lista = null;

		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			lista = session.createQuery("FROM MCApp").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}

		return lista;
	}

	/** Returns a MCAPP object given an appID
	 * @param appId
	 * @return
	 */
	public MCApp getMCApp(String appId) {
		// It is received a string with the following format: "AppId.camel" in
		// order to fulfill the requirements of the
		// xtext web server
		Session session = null;
		Transaction tx = null;
		MCApp obj = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();

			Query query = session.createQuery("FROM MCApp WHERE id = :id ");
			query.setParameter("id", new Long(appId));
			List resultList = query.getResultList();
			// MCP
			if (resultList.size() > 0)
				obj = (MCApp) resultList.get(0);
			else
				obj = null;
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
		return obj;
	}

	public MCApp getMCAppNew(String MCApplication) {
		Session session = null;
		Transaction tx = null;
		MCApp obj = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			int len = MCApplication.indexOf(fileExtension);
			String name = MCApplication.substring(0, len);
			Query query = session.createQuery("FROM MCApp WHERE MCApplication = :mcapp ");
			query.setParameter("mcapp", name);
			List resultList = query.getResultList();
			obj = (MCApp) resultList.get(0);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
		return obj;
	}

	/** Method to UPDATE camel for a mcApp
	 * @param mcAppID
	 * @param camel
	 * @param camelReport
	 * @param errorsCamelReport
	 */
	public void updateMCApp(Long mcAppID, String camel, String camelReport, int errorsCamelReport) {
		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			MCApp obj = (MCApp) session.get(MCApp.class, mcAppID);
			obj.setCamel(camel);
			obj.setCamelReport(camelReport);
			obj.setErrorsCamelReport(errorsCamelReport);
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
	}

	/** Method to UPDATE camel for a mcApp
	 * @param mcAppID
	 * @param camelXMI
	 */
	public void updateMCAppXMI(Long mcAppID, String camelXMI) {
		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			MCApp obj = (MCApp) session.get(MCApp.class, mcAppID);
			obj.setCamelXMI(camelXMI);
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
	}	/** Method to DELETE a record
	 * @param mcAppID
	 */
	public void deleteMCApp(Long mcAppID) {
		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			MCApp obj = (MCApp) session.get(MCApp.class, mcAppID);
			session.delete(obj);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
	}

	/** Method to UPDATE camel for a mcApp
	 * @param mcAppID
	 * @param statusCamelReport
	 */
	public void updateStatusMCApp(Long mcAppID, int statusCamelReport) {
		Session session = null;
		Transaction tx = null;
		try {
			session = factory.openSession();

			tx = session.beginTransaction();
			MCApp obj = (MCApp) session.get(MCApp.class, mcAppID);
			if (obj != null) {
				obj.setStatusCamelReport(statusCamelReport);
				session.update(obj);
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			if (session != null)
				session.close();
		}
	}

}